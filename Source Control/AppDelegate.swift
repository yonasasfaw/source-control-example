//
//  AppDelegate.swift
//  Source Control
//
//  Created by Yonas Asfaw on 4/19/19.
//  Copyright © 2019 Yonas Asfaw. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

